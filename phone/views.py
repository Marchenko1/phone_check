from django.shortcuts import render
from .models import Filters
from django.db.models import Q


def index(request):

    if request.method == 'GET':
        return render(request=request, template_name='phone/index.html')

    if request.method == 'POST':
        phone = request.POST.get('phone')

        if len(phone) != 11 or not phone.isdecimal():
            context = {
                'error': 'Вводите только цифры в формате 7**********',
                'phone': phone
            }
            return render(request=request, template_name='phone/index.html', context=context)

        try:
            info_object = Filters.objects.filter(
                Q(abc_def=phone[1:4]) & Q(number_from__lte=int(phone[4:11])) & Q(number_to__gte=int(phone[4:11]))).get()

            context = {
                'phone': phone,
                'number_info': {
                    'mobile_operator': info_object.mobile_operator,
                    'inn': info_object.inn,
                    'region': info_object.region,
                }
            }

            return render(request=request, template_name='phone/index.html', context=context)

        except BaseException:
            context = {
                'error': 'Номер в базе не найден',
                'phone': phone
            }

            return render(request=request, template_name='phone/index.html', context=context)


