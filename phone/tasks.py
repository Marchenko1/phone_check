from __future__ import absolute_import

import csv
import os
import requests

from celery import shared_task
from urllib3 import disable_warnings
from urllib3.exceptions import InsecureRequestWarning

from .models import Filters


@shared_task
def add():
    list_num = ['3', '4', '8', '9']

    disable_warnings(InsecureRequestWarning)
    for num in list_num:
        if num == '9':
            url = f'https://opendata.digital.gov.ru/downloads/DEF-{num}xx.csv'
        else:
            url = f'https://opendata.digital.gov.ru/downloads/ABC-{num}xx.csv'
        response = requests.get(url=url, verify=False)

        with open(os.path.join('static', 'csv', f'ABC-{num}xx.csv'), 'wb') as file:
            file.write(response.content)


@shared_task
def add_new_filter():
    list_num = ['3', '4', '8', '9']
    for i in list_num:
        path_file = os.path.join('static', 'csv', f'ABC-{i}xx.csv')
        with open(path_file, 'r', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=';', quotechar=' ')
            first_lap = True

            for row in reader:
                if first_lap:
                    first_lap = False
                    continue

                abc_def = int(row[0])
                number_from = int(row[1])
                number_to = int(row[2])
                mobile_operator = row[4]
                region = row[5]
                inn = row[6]

                filter_database = Filters.objects.filter(abc_def=abc_def) & Filters.objects.filter(number_from=number_from)
                print(f'abc_def={abc_def}')
                if filter_database.count() > 0:
                    continue
                filter_phone = Filters.objects.create(
                    abc_def=abc_def,
                    number_from=number_from,
                    number_to=number_to,
                    mobile_operator=mobile_operator,
                    region=region,
                    inn=inn
                )
                filter_phone.save()
