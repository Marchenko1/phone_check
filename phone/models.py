from django.db import models


class Filters(models.Model):
    abc_def = models.IntegerField()
    number_from = models.IntegerField()
    number_to = models.IntegerField()
    mobile_operator = models.CharField(max_length=128)
    region = models.CharField(max_length=128)
    inn = models.CharField(max_length=128)
